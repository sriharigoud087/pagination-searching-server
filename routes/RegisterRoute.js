const express = require("express");
const router = express.Router();

const registerController = require("../controller/RegistrationController");

router.post("/store", registerController.store);

router.get("/index", registerController.index);
router.get("/getPagination", registerController.getPagination);

router.get("/Search", registerController.Search);

module.exports = router;