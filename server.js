const express = require("express");
const mongoose = require("mongoose");
const morgan = require("morgan");
const bodyParser = require("body-parser");
const registerRoute = require("./routes/RegisterRoute");

mongoose.connect("mongodb://localhost:27017/userDataDb", {
    useNewUrlParser: true,
    useUnifiedTopology: true,
});
mongoose.set("useFindAndModify", false);
const db = mongoose.connection;
db.on("error", (err) => {
    console.log(err);
});

db.on("open", () => {
    console.log("Data base connected");
});

const app = express();

app.use(morgan("dev"));
app.use(express.urlencoded({ extended: true }));
app.use(express.json());

const PORT = process.env.PORT || 3000;
app.listen(PORT, () => {
    console.log(`server is running on port ` + PORT);
});
app.use("/api/register", registerRoute);