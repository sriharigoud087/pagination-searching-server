const { response } = require("express");
const Register = require("../model/Registration");

//display the list

const index = (req, res, next) => {
    Register.find()
        .then((response) => {
            //console.log(response);
            res.json({
                response,
            });
        })
        .catch((error) => {
            res.json({
                message: "An Errr Occured",
            });
        });
};

//store an Employee
const store = (req, res, next) => {
    let body = [];
    var stud;
    let register;

    req
        .on("data", (chunk) => {
            body.push(chunk);
        })
        .on("end", () => {
            body = Buffer.concat(body).toString();

            stud = JSON.parse(body);
            console.log(stud);
            register = new Register({
                userName: stud.userName,
                phoneNumber: stud.phoneNumber,
                email: stud.email,
                address: stud.address,
                city: stud.city,
                state: stud.state,
                postalCode: stud.postalCode,
            });
            register
                .save()
                .then((response) => {
                    //console.log(response);
                    res.json({
                        message: "Added succesfully",
                    });
                })
                .catch((err) => {
                    res.json({
                        message: err,
                    });
                });
            //console.log(stud.firstName);
        });
    //console.log(stud.firstName);
};

const getPagination = (req, res, next) => {
    var pageNo = parseInt(req.query.pageNo);
    var size = parseInt(req.query.size);
    console.log(pageNo);
    console.log(size);
    var query = {};
    if (pageNo < 0 || pageNo === 0) {
        response = {
            error: true,
            message: "invalid page number, should start with 1",
        };
        return res.json(response);
    }
    query.skip = size * (pageNo - 1);
    query.limit = size;
    // Find some documents
    Register.find({}, {}, query, function(err, data) {
        // Mongo command to fetch all data from collection.

        if (err) {
            response = { error: true, message: "Error fetching data" };
        } else {
            // res.json(data);
        }
        res.json(data);
    });
};

const Search = (req, res, next) => {
    var searchText = req.query.Text;

    console.log(searchText);
    Register.find({
            userName: { $regex: searchText, $options: "i" },
        })
        .then((response) => {
            console.log("heyyy" + response);
            res.json({
                response,
            });
        })
        .catch((error) => {
            res.json({
                message: "Eror Occured",
            });
        });
};

module.exports = {
    store,
    index,
    getPagination,
    Search,
};