const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const registrationSchema = new Schema({
    userName: {
        type: String,
    },
    phoneNumber: {
        type: String,
    },
    email: {
        type: String,
    },
    address: {
        type: String,
    },
    city: {
        type: String,
    },
    state: {
        type: String,
    },
    postalCode: {
        type: String,
    },
}, { timestamps: true });

const Registration = mongoose.model("Registration", registrationSchema);
module.exports = Registration;